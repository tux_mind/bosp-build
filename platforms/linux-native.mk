.PHONY:
ifndef GCC_VERSION
GCC_VERSION := $(shell gcc -dumpversion | cut -f1,2 -d.)
GCC_VERSION_3 := $(shell gcc -dumpversion |cut -f1,2,3 -d.)
ifeq (0, $(shell if [ `which gcc-$(GCC_VERSION_3)` ]; then echo 0; else echo 1; fi ))
CPP := `which "cpp-$(GCC_VERSION_3)"`
CXX := `which "g++-$(GCC_VERSION_3)"`
CC  := `which "gcc-$(GCC_VERSION_3)"`
else
ifeq (0, $(shell if [ `which gcc-$(GCC_VERSION)` ]; then echo 0; else echo 1; fi ))
CPP := `which "cpp-$(GCC_VERSION)"`
CXX := `which "g++-$(GCC_VERSION)"`
CC  := `which "gcc-$(GCC_VERSION)"`
else
CPP := `which cpp`
CXX := `which g++`
CC  := `which gcc`
endif # ifeq (0,$(shell which gcc-$(GCC_VERSION) &>/dev/null;echo $$?))
endif # ifeq (0,$(shell which gcc-$(GCC_VERSION_3) &>/dev/null;echo $$?))
else
CPP := `which "cpp-$(GCC_VERSION)"`
CXX := `which "g++-$(GCC_VERSION)"`
CC  := `which "gcc-$(GCC_VERSION)"`
endif # ifndef GCC_VERSION

#$(warning Using CGG version $(GCC_VERSION))

# Configure GCC compiler version
GCC_MAJOR := $(shell gcc -dumpversion | cut -f1 -d.)
GCC_MINOR := $(shell gcc -dumpversion | cut -f2 -d.)
GCC_TAG   := $(GCC_MAJOR)$(GCC_MINOR)

TARGET_FLAGS := "-march=native"
ifdef CONFIG_TARGET_LINUX_NATIVE_I386
TARGET_FLAGS := "-march=native -m32"
endif

PLATFORM_HAS_PARSEC := 1
platform_check:
platform_setup:
platform_distclean:
