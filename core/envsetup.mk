
# Compilation configuration
CORES := $(shell grep processor /proc/cpuinfo | wc -l)
CPUS := $(shell echo "$(CORES)" | bc)
MACH := $(shell uname -m)

# CMake configuration
BOSP_CMAKE_MODULES := "$(BUILD_DIR)/usr/share/bbque/.cmake"
CMAKE_COMMON_OPTIONS = \
		-DCMAKE_CXX_COMPILER=$(CXX)                 \
		-DCMAKE_CXX_FLAGS=$(TARGET_FLAGS)           \
		-DCMAKE_C_COMPILER=$(CC)                    \
		-DCMAKE_C_FLAGS=$(TARGET_FLAGS)             \
		-DCMAKE_BUILD_TYPE=$(BUILD_TYPE)            \
		-DCMAKE_INSTALL_PREFIX=$(BUILD_DIR)         \
		-DBOSP_CMAKE_MODULES=$(BOSP_CMAKE_MODULES)  \
		-DGCC_VERSION=\"$(GCC_MAJOR).$(GCC_MINOR)\" \
		-DGCC_TAG=$(GCC_TAG)                        \
		-DCONFIGPATH=$(KCONFIG_OUTPUT_CMAKE)        \
		-DBASE_DIR=$(BASE_DIR)

setup_build: $(BUILD_DIR)/usr/bin
$(BUILD_DIR)/usr/bin:
	@echo
	@echo "==== Setup build directory [$(BUILD_DIR)] ===="
	@mkdir -p \
		$(BUILD_DIR)/bin \
		$(BUILD_DIR)/etc/bbque/recipes \
		$(BUILD_DIR)/etc/default \
		$(BUILD_DIR)/include/bbque \
		$(BUILD_DIR)/lib/bbque \
		$(BUILD_DIR)/lib/python \
		$(BUILD_DIR)/sbin \
		$(BUILD_DIR)/share/bbque \
		$(BUILD_DIR)/usr/bin \
		$(BUILD_DIR)/usr/sbin \
		$(BUILD_DIR)/usr/share/bbque/.cmake \
		$(BUILD_DIR)/usr/python \
		$(BUILD_DIR)/var/lock \
		$(BUILD_DIR)/var/run \
		$(BUILD_DIR)/var/bbque/ocl \
		>/dev/null 2>&1
	@echo "Build dir: "$(BUILD_DIR)
	@echo "Build CPUs: "$(CPUS)
	@echo

# This check is required only for native compilation
# In the other cases a proper cross-compiler is produced by this build system.
ifdef CONFIG_TARGET_LINUX_NATIVE

# This should define the minimum required version, _minus_ 1 for the patch level
GCC_MIN_VER := 4.7
GCC_CUR_VER := $(shell $(CXX) -dumpversion)

# Build numerical version of required and current GCC version
# This addresses the possibility of double-digit numbers in any of the version
# part, and possibility of missing 3-rd part of the version in output of gcc
# -dumpversion (which is the case in some earlier gcc versions).
# Ref: http://stackoverflow.com/a/17947005
GCC_MIN_VER_NR := $(shell expr \
	`echo "$(GCC_MIN_VER)" |   \
		sed -e 's/\.\([0-9][0-9]\)/\1/g' -e 's/\.\([0-9]\)/0\1/g' -e 's/^[0-9]\{3,4\}$$/&00/'`)
GCC_CUR_VER_NR := $(shell expr \
	`echo "$(GCC_CUR_VER)" |   \
		sed -e 's/\.\([0-9][0-9]\)/\1/g' -e 's/\.\([0-9]\)/0\1/g' -e 's/^[0-9]\{3,4\}$$/&00/'`)

endif # CONFIG_TARGET_LINUX_NATIVE

setup_check: platform_check
	@echo
	@echo "==== Checking building system dependencies ==="
	@echo -n "Checking for git.......... "
	@which git || (\
		echo "MISSING: install [git-core,gitk,git-gui]" && \
		exit 1)
	@echo -n "Checking for cmake........ "
	@which cmake || (echo "MISSING: install [cmake]" && exit 1)
	@echo -n "Checking for configure.... "
	@which autoconf || (echo "MISSING: install [autoconf]" && exit 1)
	@echo -n "Checking for autoreconf... "
	@which autoreconf || (echo "MISSING: install [autoconf]" && exit 1)
	@echo -n "Checking for libtoolize... "
	@which libtoolize || (echo "MISSING: install [libtool]" && exit 1)
	@echo -n "Checking for make......... "
	@which make || (echo "MISSING: install [automake]" && exit 1)
	@echo -n "Checking for doxygen...... "
	@which doxygen || (\
		echo "Missing 'doxygen': install [doxygen]" && \
		exit 1)
ifdef CONFIG_TARGET_LINUX_NATIVE
# Uncomment this and indent for numerical version check debugging
# @echo "GCC min ver: $(GCC_MIN_VER) ($(GCC_MIN_VER_NR))"
# @echo "GCC cur ver: $(GCC_CUR_VER) ($(GCC_CUR_VER_NR))"
	@echo -n "Checking for gcc.......... "
	@if [ $(GCC_MIN_VER_NR) -le $(GCC_CUR_VER_NR) ]; then \
		echo "$(GCC_CUR_VER) ($(CXX))"; \
	else \
		echo 'FAILED\n'; \
		echo "   Current GCC version      : ${GCC_CUR_VER}      ($(CXX))"; \
		echo "   Min required GCC version : ${GCC_MIN_VER}\n\n"; \
		exit 1; \
	fi

endif # CONFIG_TARGET_LINUX_NATIVE
	@echo
	@echo "Check SUCCESS: all required tools are available."
	@echo

clean_out:
	@echo
	@echo "==== Clean-up build directory [$(BUILD_DIR)] ===="
	@[ -d $(BASE_DIR)/out ] && \
		rm -rf $(BASE_DIR)/out 2>/dev/null || \
		exit 0


