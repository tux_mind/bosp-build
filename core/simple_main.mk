################################################################################
# Copyright (C) 2012  Politecnico di Milano
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
################################################################################

################################################################################
# Building Targets
################################################################################

.PHONY: all setup build clean

# Main BOSP build targets
all: setup build

# BOSP Configuration setup
setup: setup_config setup_build setup_check

# Dependency collection targets, all configuration enabled targets should be
# added to the build and clean targets
build: external bbque testing contrib benchmarks
clean: clean_bbque clean_testing clean_contrib clean_benchmarks clean_external

distclean: clean platform_distclean clean_out clean_config


################################################################################
# Build Configuration Setup (using KConfig)
################################################################################

include build/core/config.mk
clean_config:
	@echo
	@echo "==== Clean-up BOSP configuration ===="
	@rm build/configuration/bosp-config* 2>/dev/null || echo -n


################################################################################
# Platform Specific Definitions
################################################################################

include build/platforms/platform.mk
include deployment/deployment.mk


################################################################################
# Build environment setup
################################################################################

include build/core/envsetup.mk


################################################################################
# External project
################################################################################

.PHONY: external clean_external
external:
clean_external:

-include build/core/external-required.mk
-include build/core/external-tools.mk
-include build/core/external-optional.mk
-include build/core/external-partial.mk

################################################################################
# Build Barbeque RTRM
################################################################################

include barbeque/bosp.mk


################################################################################
# Testing projects
################################################################################

.PHONY: testing clean_testing
testing:
clean_testing:

-include build/core/contrib-testing.mk


################################################################################
# Contributed projects
################################################################################

.PHONY: contrib clean_contrib
contrib: bbque
clean_contrib:

-include build/core/contrib-user.mk


################################################################################
# Tools projects
################################################################################

.PHONY: tools clean_tools
tools: bbque
clean_tools:

-include build/core/tools.mk


################################################################################
# Benchmarks
################################################################################

.PHONY: benchmarks clean_benchmarks
benchmarks: bbque
clean_benchmarks:

-include build/core/benchmarks-modules.mk
