#!/bin/bash

# Loading input parameters
PROJECT=${1:-barbeque}
ACTION=${2:-conf}
BRANCH=${3:-drk-rtlibStats}
PLATFORM=${4:-android}
GCC=${5:-4.6}
PIL=${6:-pp}
DBG=${7:-rel}

# The repo script used to enumerate BOSP projects
REPO=build/buildbot/scripts/repo
# The script to convert KConfig to CMake
KCSCRIPT=external/kconfig/makefile_to_cmake.sed

# Custom log formatter
function log() {
  echo "[BB] " $*
}

if [ x"${ACTION:0:6}" == x"build_" ]; then
	TARGET=${ACTION:6:100}
	ACTION=${ACTION:0:5}
fi

log "============================"
log "===== BuildBot Configuration"
log "Project: $PROJECT"
log "Action : $ACTION"
log "Target : $TARGET"
log "Branch : $BRANCH"
log "Target : $PLATFORM"
log "GCC    : $GCC"
log "PIL    : $PIL"
log "DBG    : $DBG"
log "============================"

PRJLIST=""
function get_projects() {
  PRJLIST=$(${REPO} forall -c 'pwd | sed -e "s/\/home\/opt\/BOSP\///"');
}

function do_fetch() {
  log "Fetching all projects..."
  ${REPO} forall -c git fetch -v bosp-i7
}


# The pre-defined configuration
# NOTE: so far we do not have different configurations based on compiler
# version. Thus, we force compiler version to 4.6.
#CONF=${PLATFORM}_gcc-${GCC}_${PIL}_${DBG}
CONF=${PLATFORM}_gcc-4.6_${PIL}_${DBG}
CONFDIR=build/configuration/configs
SRCCONF=${CONFDIR}/${CONF}_defconfig
# The target configuration
DESTDIR=build/configuration
DSTCONF=${DESTDIR}/bosp-config

function do_load_conf() {
  if [ ! -f $SRCCONF ]; then
    log "Missing required pre-configuration [$CONF]"
    return 1
  fi
  log "Switch to pre-defined [$CONF] configuration..."
  cp $SRCCONF $DSTCONF
  cat $DSTCONF | sed -f $KCSCRIPT > ${DSTCONF}.cmake
}

function do_switch() {
  CURB=$(cat $1/.git/HEAD | sed 's/ref: refs\/heads\///')
  TRGB=$BRANCH

  # Clean-up working dir to be ready for a switch
  pushd $1
  git reset --hard HEAD

  # check for platform-specific branch
  if [ -f .git/refs/remotes/bosp-i7/${BRANCH}_${PLATFORM} ]; then
    TRGB=${BRANCH}_${PLATFORM}
  elif [ -f .git/refs/remotes/bosp-i7/${CURB}_${PLATFORM} ]; then
    TRGB=${CURB}_${PLATFORM}
  fi

  # if we are already on that branch: just pull new commits
  if [ "x$CURB" == "x$TRGB" ]; then
    log "Project [$1] already on [$TRGB] branch, just pull new commits..."
    git pull bosp-i7 $CURB
    popd
    return
  fi

  # if the required branch does not exist: do nothing
  if [ ! -f .git/refs/remotes/bosp-i7/$TRGB ]; then
    log "Project [$1] keept on [$CURB] branch, just pull new commits..."
    git pull bosp-i7 $CURB
    popd
    return
  fi

  # switching to new branch
  if [ -f .git/refs/heads/$TRGB ]; then
    git checkout $TRGB
    git pull bosp-i7 $TRGB
  else
    git checkout -b $TRGB bosp-i7/$TRGB
  fi
  popd
  log "Project [$1] switched to [$TRGB] branch..."
}

function do_switch_all() {
  log "Switching BOSP projects to [$BRANCH] branch..."
  for P in $PRJLIST; do
    do_switch $P
  done
}

function do_purge() {
  log "Purging build dir..."
  make distclean
}

function do_configure() {

  # Load pre-configuration
  do_load_conf
  if [ $? -ne 0 ]; then
	  log "Configuration FAILED"
	  return 1
  fi

  # Update the list of tracked projects
  get_projects

  # Fetch all updates
  do_fetch

  # Switch to required branch
  do_switch_all

  # Read previous configuration
  if [ -f out/bbTarget ]; then
    PPLAT=$(cat out/bbTarget)
    PGCC=$(cat out/bbGcc)
  fi
  # Check if the current built should be purged
  if [ "x$PLAT" == "x$PPLAT" -a "x$GCC" == "x$PGCC" ]; then
	  log "Same target and compiler: keeping out dir"
	  exit 0
  fi

  # Clean-up the build folder
  do_purge

  # Reload pre-configuration (wiped by do_purge)
  do_load_conf

  # Keep track of current configuration
  mkdir out 2>/dev/null
  echo $PLAT > out/bbTarget
  echo $GCC > out/bbGcc

  return 0

}

function do_build() {
  log "Building [$1] target..."
  GCC_VERION=${GCC} make $1
}

case $ACTION in
conf)
  do_configure && exit 0
  exit 1
  ;;
build)
  do_build $TARGET && exit 0
  exit 1
  ;;
esac

exit 1

